<?php namespace App\Controllers;


use App\Models\VehiculoModel;


class Vehiculo extends BaseController
{


	public $vehiculoModel;


	public function __construct()
	{

		if (!session('usuario')) {

			return redirect()->to(base_url());
		}


		$this->vehiculoModel = new VehiculoModel();

	}


	function filtrar()
	{


		$placa = $this->request->getPost("placa");


		if (strlen($placa) > 2) {


			$filtro = $this->vehiculoModel->filtrar($placa);




		}else{
            $filtro = $this->vehiculoModel->filtrar(null);

        }
        echo json_encode($filtro);

	}


	function mostrar()
	{


		$placa = $this->request->getGetPost("placa");


		$vehiculos = $this->vehiculoModel->consultar($placa);

		echo json_encode($vehiculos);

	}


	function listar()
	{

		$vehiculos = $this->vehiculoModel->filtrar("all");
		echo json_encode($vehiculos);

	}


	function consultar($placa = null)
	{


		return $this->vehiculoModel->consultar($placa);


	}


	function mostrarPlacas()
	{


		echo json_encode($this->vehiculoModel->findColumn("placa"));

	}


	function crear()
	{

		$datos = get_post();

		$existe = $this->consultar($datos['placa']);
		count($existe) > 0 ? $datos['ultimaFechaActualizacion'] = get_now() : $datos['fechaRegistro'] = get_now();
        //echo json_encode($datos);

		echo $this->vehiculoModel->save($datos);


	}


	function xxx()
	{


		var_dump($this->vehiculoModel->consultar2("PBC355"));


	}


}
