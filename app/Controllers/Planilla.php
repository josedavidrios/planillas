<?php namespace App\Controllers;

use App\Models\PlanillaModel;
use App\Libraries\Pdf;


class Planilla extends BaseController
{


	public $planillaModel;


	public function __construct()
	{


		if (!session('usuario')) {

			return redirect()->to(base_url());
		}


		$this->planillaModel = new PlanillaModel();

	}


	function filtrar()
	{


		$placa = $this->request->getPost("placa");


		if (strlen($placa) > 1) {


			$filtro = $this->planillaModel->filtrar($placa);


			foreach ($filtro as $fil) {

				$fil->fecha = fechas_es($fil->fecha);
				$fil->pagada = badge_si_no($fil->pagada);


			}


			echo json_encode($filtro);

		}


	}


	function mostrarTiposPlanillas()
	{


		$filtro = $this->planillaModel->consultarTiposPlanillas();


		echo json_encode($filtro);

	}


	function pagar()
	{

		$numero = $this->request->getGetPost("numero");

		return $this->planillaModel->set(['pagada' => 1])
			->where(['numero' => $numero])
			->update();


	}


	function consultar()
	{


		$codigo = $this->request->getGetPost("placa");


		$usuarios = $this->planillaModel->consultar($codigo);

		echo json_encode($usuarios);

	}


	function crear2()
	{


		$datos = get_post();
		$placas = explode(",", $datos['placas']);

		$vehiculo = new Vehiculo();


		$cant = 0;




		foreach ($placas as $placa) {


			$aux = $vehiculo->consultar($placa);





			$datosNuevaPlanilla = [

				"fecha" => $datos['fecha'],
				"identificacionConductor" => $aux[0]->identificacionConductorPorDefecto,
				"placaVehiculo" => $aux[0]->placa,
				"tipo" => $aux[0]->tipoPlanitllaPorDefecto,
				"pagada" => 0,
				"fechaCreacion" => get_now(),
                "identificacionAdmistrador"=>session('identificacion')


			];


			$numero = $this->planillaModel->crear($datosNuevaPlanilla);
			$this->crearDetallePlanillas($numero);

			$cant++;

		}


		echo $cant;

	}


	function crearDetallePlanillas($numero){


        $rutas =["ST","TS"];

        for ($i=0;$i<count($rutas);$i++){


            $datosDetalleNuevaPlanilla=[

                "numeroPlanilla"=>$numero,
                "codigoRuta"=>$rutas[$i]

            ];

            $this->planillaModel->crearDetallePlanilla($datosDetalleNuevaPlanilla);


        }




    }



	function crear()
	{

		$datos = get_post();

		$datos['fechaRegistro'] = get_now();

		$fechaInicio = strtotime($datos['fechaInicio']);

		$fechas = [];

        $rutas =["ST","TS"];

		if (isset($datos['fechaFin'])) {


			$fechaFin = strtotime($datos['fechaFin']);

			for ($i = $fechaInicio; $i <= $fechaFin; $i += 86400) {


				$aux = date("Y-m-d", $i);
				array_push($fechas, $aux);

			}

		} else {

			$fechas = [$datos['fechaInicio']];

		}


		$vehiculo = new Vehiculo();

		for ($i = 0; $i < count($fechas); $i++) {

			foreach ($vehiculo->consultar() as $aux) {

				$datosNuevaPlanilla = [

					"fecha" => $fechas[$i],
					"identificacionConductor" => $aux->identificacionConductorPorDefecto,
					"placaVehiculo" => $aux->placa,
					"tipo" => $aux->tipoPlanitllaPorDefecto,
					"pagada" => 0,
					"fechaRegistro" => get_now(),
                    "identificacionAdmistrador"=>session('identificacion')

				];

                $numero=	$this->planillaModel->crear($datosNuevaPlanilla);
               $this->crearDetallePlanillas($numero);


            }

		}


	}




	function generar()
	{


		$placa = $this->request->getGetPost('placa');


		$planillas = $this->planillaModel->consultarPor("placaVehiculo", $placa);

		header('Content-type: application/pdf');

		$pdf = new Pdf('P', 'cm', [8, 11]);

		$pdf->SetTitle("PLANILLA DE VIAJE");


		foreach ($planillas as $planilla) {


			$pdf->AddPage();


			$pdf->SetFont('Arial', '', 10);

		//	$pdf->Image(base_url('azzara/assets/img/logo-supertransporte.png'), 1, 1, 5100 / 1000, 2800 / 1000, "PNG", "");


			$pdf->Image(base_url('azzara/assets/img/cooperativa2.png'), 0, 0, 1220 / 150, 362 / 150, "PNG", "");

			$pdf->SetXY(0.8, 2.2);
			$pdf->Cell(1, 1, 'NIT: 823003263-1 | TEL: 3016879718');
			$pdf->SetXY(0.85, 2.8);
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(1, 1, 'DIAGONAL 2 6 93, TOLUVIEJO, SUCRE');



			$pdf->SetFont('Arial', '', 10);

			$pdf->Ln(1);
			$pdf->Cell(0, 0, 'PLANILLA DE VIAJE ' . utf8_decode('N° ' . str_pad($planilla->numero, 5, "0", STR_PAD_LEFT)),0,1,'C');
			$pdf->SetFont('Arial', '', 9);
			$pdf->Ln(1);

			$pdf->Cell(0, 0, 'FECHA: ' . $planilla->fecha,0,1,'C');


			$pdf->Ln(1);

			$pdf->Cell(0, 0, 'RUTA: '.$planilla->ruta, 0, 1, 'C');


			$pdf->Ln(1);



			$pdf->Cell(0, 0, 'DONCUCTOR PRICIPAL', 0, 1, 'C');

			$pdf->Ln(1);

			//   $pdf->SetXY(2,7);
			//    $pdf->SetXY(2,8);


			$pdf->Cell(0, 0, $planilla->conductor, 0, 1, 'C');
			// $pdf->Cell(1,1,'');


		}


		$pdf->Output();

        exit;


	}


	function comprobanteDeEgreso()
	{


		helper('time');

		header('Content-type: application/pdf');

		$pdf = new Pdf('P', 'cm');
		$pdf->SetTitle("COMPROBANTE DE EGRESOS");
		$pdf->AddPage();





		$pdf->SetFont('Arial', 'B', 12);
//		$pdf->Image(base_url('azzara/assets/img/cooperativa.png'), 2, 1, 1280 / 800, 1280 / 800, "PNG", "");

		$pdf->SetXY(3.6, 1);
		$pdf->Cell(1, 1, 'COOPERATIVA DE TRANSPORTES MIXTO DE TOLUVIEJO - COOTRAMIXTOL');

		$pdf->Ln(1);


		$pdf->Cell(0, 0, 'NIT: 823003263-1', 0, 1, 'C');
		$pdf->Ln(2);

		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(0, 0, 'COMPROBANTE DE INGRESOS', 0, 1, 'L');


		$yComprobante = 3.6;

		$pdf->SetFont('Arial', '', 10);
		$pdf->SetXY(12, $yComprobante);
		$pdf->Cell(1, 0.6, utf8_decode('N°'), 1, 0, 'C');

		$pdf->SetXY(13, $yComprobante);
		$pdf->Cell(1, 0.6, '69', 1, 0, 'C');

		$pdf->SetXY(14, $yComprobante);
		$pdf->Cell(1.6, 0.6, "Fecha", 1, 0, 'C');

		$pdf->SetXY(15.6, $yComprobante);
		$pdf->Cell(2.3, 0.6, get_today(), 1, 0, 'C');


		/*Tabla*/


		$pdf->SetXY(1, 6);
		$pdf->MultiCell(19, 6, "", 1, "L");



        $pdf->Line(1,6.9,20,6.9);





        $pdf->SetFont('Arial', 'B', 10);

        $yTitulosTabla1=6.1;
        $pdf->SetXY(1, $yTitulosTabla1);
        $pdf->Cell(2.3, 0.6, "POR CONCEPTO DE", 0, 0, 'L');


        $pdf->SetXY(5, $yTitulosTabla1);
        $pdf->Cell(2.3, 0.6,  utf8_decode("DÍA ANT."), 0, 0, 'L');


        $pdf->SetXY(7, $yTitulosTabla1);
        $pdf->Cell(2.3, 0.6,  utf8_decode("DÍA"), 0, 0, 'L');


        $pdf->SetXY(9, $yTitulosTabla1);
        $pdf->Cell(2.3, 0.6,  "VR UNIT.", 0, 0, 'L');

        $pdf->SetXY(15, $yTitulosTabla1);
        $pdf->Cell(2.3, 0.6,  "VR. TOTAL", 0, 0, 'L');




        $pdf->SetFont('Arial', '', 9);


        $planillas = $this->planillaModel->consultarPlanillado("2020-04-02");


        $i=$yTitulosTabla1-5;

        $cantPlanillas = 0;
        $valorTotalPlanillas = 0;

        foreach ($planillas as $planilla){

            $pdf->SetXY(1, $yTitulosTabla1+$i);
            $pdf->Cell(2.3, 0.6, "Planillas de ".number_format ($planilla->valor,2), 0, 0, 'L');


            $pdf->SetXY(7, $yTitulosTabla1+$i);
            $pdf->Cell(2.3, 0.6,  $planilla->cant, 0, 0, 'L');


            $pdf->SetXY(15, $yTitulosTabla1+$i);
            $pdf->Cell(2.3, 0.6, number_format($planilla->total,2) , 0, 0, 'L');

            $cantPlanillas+=$planilla->cant;
            $valorTotalPlanillas+=$planilla->total;

            $i++;
        }
        $pdf->SetFont('Arial', 'B', 9);

        $pdf->Line(1,11.2,20,11.2);

        $pdf->SetXY(1, 11.3);
        $pdf->Cell(2.3, 0.6, "TOTALES", 0, 0, 'L');

        $pdf->SetXY(7, 11.3);
        $pdf->Cell(2.3, 0.6,  $cantPlanillas, 0, 0, 'L');

        $pdf->SetXY(15, 11.3);
        $pdf->Cell(2.3, 0.6,  number_format($valorTotalPlanillas,2), 0, 0, 'L');


        $pdf->SetFont('Arial', '', 9);

        $xLinea1 = 5;

        $y2LineTabla=12;

		$pdf->Line($xLinea1, 6, $xLinea1, $y2LineTabla);  //Linea vertical 1

		$xLinea2 = 7;
		$pdf->Line($xLinea2, 6, $xLinea2, $y2LineTabla);  //Linea vertical 1

		$xLinea2 = 9;
		$pdf->Line($xLinea2, 6, $xLinea2, $y2LineTabla);  //Linea vertical 1



		$xLinea4 = 15;
		$pdf->Line($xLinea4, 6, $xLinea4, $y2LineTabla);  //Linea vertical 1





		$pdf->Output();

		exit;


	}


	function xx()
	{

	    echo var_dump($this->planillaModel->consultarPlanillado("2020-04-02"));


    }


}
