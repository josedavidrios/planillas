<?php namespace App\Controllers;


use App\Models\ConductorModel;


class Conductor extends BaseController
{


	public $conductorModel;


	public function __construct()
	{

		if (!session('usuario')) {

			return redirect()->to(base_url());
		}


		$this->conductorModel = new ConductorModel();

	}


	function consultar($identificacion=null)
	{

		return $this->conductorModel->consultar($identificacion);


	}


	function filtrar()
	{


		$nombres = $this->request->getPost("nombres");


		if (strlen($nombres) > 1) {




			$filtro = $this->conductorModel->filtrar($nombres);

            foreach ($filtro as $fil) {


                $fil->activo = badge_si_no($fil->activo);


            }



            echo json_encode($filtro);

		}


	}


    function mostrarTodo()
    {



        $conductores = $this->conductorModel->consultar();



        foreach ($conductores as $fil) {


            $fil->activo = badge_si_no($fil->activo);


        }


        echo json_encode($conductores);

    }



    function mostrar()
	{

		$identificacion = $this->request->getGetPost("identificacion");
		$activo = $this->request->getGetPost("activo");


		$conductores = $this->conductorModel->consultar($identificacion,$activo);


		/*
        foreach ($conductores as $fil) {


            $fil->activo = badge_si_no($fil->activo);


        }

		*/


		echo json_encode($conductores);

	}


	function crear()
	{

		$datos = get_post();


		$existe = $this->consultar($datos["identificacion"]);
		count($existe) > 0 ? $datos['ultimaFechaActualizacion'] = get_now() : $datos['fechaRegistro'] = get_now();


		echo $this->conductorModel->save($datos);

	}


}
