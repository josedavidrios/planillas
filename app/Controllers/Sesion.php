<?php namespace App\Controllers;

use App\Models\SesionModel;

class Sesion extends BaseController
{

    public $sesion;
    public  $modeloSesion;

    public function __construct()
    {


        $this->modeloSesion = new SesionModel();
        $this->sesion = $session = \Config\Services::session();


    }

    public function index()
	{



        if ($this->sesion->has('usuario')) {

            return redirect()->to(base_url('Administrador'));


        }else{

            $dat['title'] = "Dashboard";
            echo view('sesion/login', $dat);


        }


    }



    function iniciar(){




        $usuario = $this->request->getPost('usuario');
        $clave = $this->request->getPost('clave');

        $users = $this->modeloSesion->iniciarSesion($usuario,$clave);


        if (count($users)){

            $datosUsuarios = [
                "identificacion" => $users[0]->identificacion,
                "usuario" => $users[0]->nombres,
                "nombres"=>$users[0]->nombres

            ];

            $this->sesion->set($datosUsuarios);

            return redirect()->to(base_url("Administrador"));


        }else{

            $this->sesion->setTempdata  ( 'error_login',1 ,  300 );

            return redirect()->to(base_url());

        }






    }


    function cerrar(){

        session_destroy();
        return redirect()->to(base_url());


    }


}
