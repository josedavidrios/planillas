<?php namespace App\Controllers;


use App\Models\DespachoModel;


class Despacho extends BaseController
{



    public $despachoModel;


    public function __construct()
    {




		if (!session('usuario')) {

			return redirect()->to(base_url());
		}

        $this->despachoModel = new DespachoModel();

    }




    function crear(){

        $datos= get_post();
        $datos['fechaRegistro']=get_now();


        echo $this->despachoModel->save($datos);

    }


    function consultar(){




        //$datos= get_post();


        $placa = $this->request->getGetPost("placa");
        $fecha = $this->request->getGetPost("fecha");



        echo json_encode($this->despachoModel->consultar($placa,$fecha));






    }







}
