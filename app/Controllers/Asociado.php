<?php namespace App\Controllers;

use App\Models\AsociadoModel;




class Asociado extends BaseController
{



    public $asociadoModel;


    public function __construct()
    {

		if (!session('usuario')) {

			return redirect()->to(base_url());
		}


        $this->asociadoModel = new AsociadoModel();

    }




    /*
    function consultar(){



        $placa =  $this->request->getPost("identificacion");

      return  json_encode($this->conductoresModel->consultar($placa));


    }

    */


    function filtrar(){


		$nombres =  $this->request->getPost("nombres");



		if (strlen($nombres)>1){


			$filtro = $this->asociadoModel->filtrar($nombres);


            foreach ($filtro as $fil) {


                $fil->activo = badge_si_no($fil->activo);


            }




            echo json_encode($filtro);

		}




	}

	function consultar($identificacion=null)
	{

		return $this->asociadoModel->consultar($identificacion);


	}

    function mostrar(){

        $identificacion =  $this->request->getGetPost("identificacion");
        $asociados =  $this->asociadoModel->consultar($identificacion);


        foreach ($asociados as $fil) {


            $fil->activo = badge_si_no($fil->activo);


        }



        echo json_encode($asociados);

    }



    function crear(){

        $datos= get_post();

		$existe = $this->consultar($datos["identificacion"]);
		count($existe) > 0 ? $datos['ultimaFechaActualizacion'] = get_now() : $datos['fechaRegistro'] = get_now();



		echo $this->asociadoModel->save($datos);

    }








}
