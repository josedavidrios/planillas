<?php namespace App\Controllers;


use App\Libraries\Pdf;

class Administrador extends BaseController
{


	public function __construct()
	{

		helper("time");
		helper(['html']);


		if (!session('usuario')) {

			return redirect()->to(base_url());
		}

	}

	function index()
	{

        if (!session('usuario')) {

            return redirect()->to(base_url());
        }


        $dat['js'] = ['app/vehiculo/vehiculo.js'];

        $dat['css'] = [''];
        $dat['title'] = "Vehículos";
        $dat['content'] = "vehiculo/inicio";


        echo view('dashboard/inc/layout', $dat);


	}


	/*
	 *
	 * Vistas
	 *
	 * */


	function vistaVehiculos()
	{


		if (!session('usuario')) {

			return redirect()->to(base_url());
		}


        $dat['js'] = ['app/vehiculo/vehiculo.js'];

		$dat['css'] = [''];
		$dat['title'] = "Vehículos";
		$dat['content'] = "vehiculo/inicio";


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaConductores()
	{

		if (!session('usuario')) {

			return redirect()->to(base_url());
		}



		$dat['css'] = ['switchery.min.css'];

        $dat['js'] = ['switchery.min.js','app/conductor/conductor.js'];


        $dat['title'] = "Conductores";
		$dat['content'] = "conductor/inicio";


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaAsociados()
	{
		if (!session('usuario')) {

			return redirect()->to(base_url());
		}

		$dat['css'] = [''];
		$dat['title'] = "Asociados";
		$dat['content'] = "asociado/inicio";


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaPlanillas()
	{
		if (!session('usuario')) {

			return redirect()->to(base_url());
		}

		$dat['css'] = [''];
		$dat['title'] = "Planillas";
		$dat['content'] = "planilla/planillado_general";


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaPlanillasEspecificas()
	{
		if (!session('usuario')) {

			return redirect()->to(base_url());
		}


		helper(['html']);

		$dat['css'] = ['vue-select.css'];
		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/planilla/planilla.js'];

		$dat['title'] = "Planillas";
		$dat['content'] = "planilla/planillado_especifico";

		//   $dat['placas'] =


		echo view('dashboard/inc/layout', $dat);

	}

	function vistaDescargarPlanilla()
	{

		if (!session('usuario')) {

			return redirect()->to(base_url());
		}

		$dat['css'] = ['vue-select.css'];

		$dat['title'] = "Planillas";
		$dat['content'] = "planilla/descargar";
		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/planilla/descargarPlanillas.js'];

		//   $dat['placas'] =


		echo view('dashboard/inc/layout', $dat);

	}


	function vistaRegistrarDespachos()
	{


		if (!session('usuario')) {

			return redirect()->to(base_url());
		}


		$dat['css'] = [''];
		$dat['title'] = "Despacho de Vehículos";
		$dat['content'] = "despacho/registro";


		echo view('dashboard/inc/layout', $dat);

	}

	function vistaConsultarDespachos()
	{

		if (!session('usuario')) {

			return redirect()->to(base_url());
		}


		$dat['css'] = ['vue-select.css'];

		$dat['title'] = "Consultar Despacho de Vehículos";
		$dat['content'] = "despacho/consultar";

		$dat['js'] = ['lodash.min.js', 'vue-select.js', 'app/despacho/consultar.js'];


		echo view('dashboard/inc/layout', $dat);

	}



	function xx(){


		$fechaInicio=strtotime("25-02-2008");
		$fechaFin=strtotime("01-04-2008");
		for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
			echo date("d-m-Y", $i)."<br>";
		}

	}

	function x(){

		$comienzo = new DateTime('19-03-2019');
		$final = new DateTime('19-04-2019');

		for ($i = $comienzo; $i <= $final; $i->modify('+1 day')) {
			echo $i->format("Y-m-d") . "\n";
		}

	}




}
