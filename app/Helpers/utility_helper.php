<?php
function asset_url($path)
{
    return base_url() . "/azzara/assets/".$path;
}



function fechas_es ($fecha) {


    $formato = substr($fecha, 0, 10);
    $numeroDia = date('d', strtotime($formato));
    $dia = date('l', strtotime($formato));
    $mes = date('F', strtotime($formato));

    $dias_ES = array("Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom");
    $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
    $nombredia = str_replace($dias_EN, $dias_ES, $dia);
    $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $nombreMes = str_replace($meses_EN, $meses_ES, $mes);


  //  return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;

    return mb_strtoupper ($nombredia." ".$numeroDia." de ".$nombreMes);
}


function badge_si_no($val){



    $format = '<span class="badge badge-danger">NO</span>';

    if ($val==1){

        $format = '<span class="badge badge-success">SÍ</span>';

    }




    return $format;


}



function get_post(){



    $datos=[];

    foreach($_POST as $campo => $valor) {



        /*

        if ( strcmp($valor,"TRUE")==0){

            $valor = 1;

        }

        if ( strcmp($valor,"FALSE")==0){

            $valor=0;

        }

        */


        $datos+=["$campo" =>  strtoupper($valor)];




    }


    return $datos;

}




