<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title> <?=$title?> </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="<?=asset_url('img/icon.ico')?>" type="image/x-icon"/>

    <!-- Fonts and icons -->
    <script src="<?=asset_url('js/plugin/webfont/webfont.min.js')?>"></script>


    <!-- CSS Files -->
    <link rel="stylesheet" href="<?=asset_url('css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=asset_url('css/azzara.css')?>">
    <link rel="stylesheet" href="<?=asset_url('css/fonts.css')?>">

</head>
<body class="login">
<div class="wrapper wrapper-login">
    <div class="container container-login animated fadeIn">
        <h3 class="text-center">Inicio de Sesión</h3>


        <form action="<?=base_url('Sesion/iniciar')?>" method="post">

        <div class="login-form">
            <div class="form-group">
                <label for="username" class="placeholder"><b>Usuario</b></label>
                <input id="username" name="usuario" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="password" class="placeholder"><b>Contraseña</b></label>
                <a href="#" class="link float-right">¿ Olvidó su contraseña ? </a>
                <div class="position-relative">
                    <input id="password" name="clave" type="password" class="form-control" required>
                    <div class="show-password">
                        <i class="flaticon-interface"></i>
                    </div>
                </div>
            </div>







            <?php



            $error_login = session("error_login");




                if (isset($error_login)){


            ?>

                    <!--
                    <div class="alert alert-danger" role="alert">
                        La contraseña es incorrecta. Vuelve a intentarlo.
                    </div>

                    -->


                    <p class="text-danger">

                        La contraseña es incorrecta. Vuelve a intentarlo.
                    </p>


            <?php

                }
            ?>







            <div class="form-group form-action-d-flex mb-3">


                <input class="btn btn-primary offset-md-7 col-md-5 float-right mt-3 mt-sm-0 fw-bold" value="Iniciar" type="submit">


            </div>


        </form>




        </div>
    </div>


<script src="<?=asset_url('js/core/jquery.3.2.1.min.js')?>"></script>
<script src="<?=asset_url('js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')?>"></script>
<script src="<?=asset_url('js/core/popper.min.js')?>"></script>
<script src="<?=asset_url('js/core/bootstrap.min.js')?>"></script>
<script src="<?=asset_url('js/ready.js')?>"></script>
</body>
</html>