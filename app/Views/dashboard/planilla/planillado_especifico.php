<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Base Form Control</div>
            </div>
            <div class="card-body">


                <form method="post" id="form" @submit.prevent="crear">

                    <div class="form-group form-inline">


                        <label for="inlineinput" class="col-md-1 col-form-label">Fecha</label>
                        <div class="col-md-3 p-0">

                            <input type="date" class="form-control w-100" required v-model="planilla.fecha">


                        </div>

                        <label for="inlineinput" class="col-md-1 col-form-label">Vehículos</label>
                        <div class="col-md-6 p-0">

                            <v-select :required="!planilla.placas" :required="re" name="placas"
                                      v-model="planilla.placas" multiple :options="listado"></v-select>


                        </div>


                        <div class="col-md-1 p-2">
                            <input type="submit" class="btn btn-success" value="Planillar">
                        </div>

                    </div>


                </form>

            </div>

        </div>

    </div>

</div>

