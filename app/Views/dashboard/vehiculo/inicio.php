<div class="row">


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-11 col-sm-11">
                <form class="navbar-left navbar-form nav-search mr-md-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-search pr-1">
                                <i class="fa fa-search search-icon"></i>
                            </button>
                        </div>
                        <input type="text" @keyup="filtar" v-model="filtro" placeholder="Buscar...."
                               class="form-control text-uppercase">

                    </div>
                </form>


            </div>

            <div class="col-md-1 col-sm-1">


                <a href="#" class="pull-right btn btn-info btn-border btn-round btn-sm" data-toggle="modal"
                   @click="abrirModal(0)">
                    <span class="btn-label"><i class="fa fa-user-plus"></i></span>Nuevo
                </a>


            </div>

        </div>


    </div>


</div>

<br>

<div class="row">


    <div class="col-md-12">


        <div class="card">

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th width="10" scope="col">#</th>
                        <th scope="col">PLACA</th>
                        <th scope="col">MARCA</th>
                        <th scope="col">TIPO</th>
                        <th scope="col">CONDUCTOR</th>

                        <th scope="col">EDITAR</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in listado">
                        <td>{{index+1}}</td>
                        <td>{{item.placa}}</td>
                        <td>{{item.marca}}</td>
                        <td>{{item.tipo}}</td>
                        <td>{{item.conductor}}</td>
                        <td width="10" class="text-center">

                            <a @click="abrirModal(item.placa)"> <i class="fas fa-edit"></i> </a>

                        </td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>





<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">


        <!--

        v-on:submit.prevent="registrarVehiculos"

        -->

        <form method="post" id="form" @submit.prevent="crear">

            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registrar y actualizar Vehículos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-md-12">


                            <div class="card-body">


                                <div class="form-group">
                                    <label>Placa</label>

                                    <input type="hidden" v-model="vehiculo.codigo" class="form-control text-uppercase">


                                    <input type="text" v-model="vehiculo.placa" class="form-control text-uppercase"
                                           placeholder="Placa">


                                </div>

                                <div class="form-group">
                                    <label>Tarjeta de Propiedad</label>
                                    <input type="text" v-model="vehiculo.tarjetaPropiedad"
                                           class="form-control text-uppercase"
                                           placeholder="Tarjeta de Propiedad">

                                </div>


                                <div class="form-group">
                                    <label>Tipo </label>

                                    <select v-model="vehiculo.tipo" class="form-control text-uppercase" required>


                                        <option value="">SELECCIONE</option>

                                        <option v-for="item in tipos" :value="item">{{item}}</option>


                                    </select>


                                </div>


                                <div class="form-group">
                                    <label>Conductor </label>


                                    <select v-model="vehiculo.conductorPorDefecto" class="form-control text-uppercase" required>


                                        <option value="">SELECCIONE</option>

                                        <option v-for="item in listadoConductores" :value="item.identificacion">{{item.nombres}}</option>


                                    </select>


                                </div>


                                <div class="form-group">
                                    <label>Marca </label>


                                    <select v-model="vehiculo.marca" class="form-control text-uppercase" required>


                                        <option value="">SELECCIONE</option>

                                        <option v-for="item in marcas" :value="item">{{item}}</option>


                                    </select>


                                </div>

                                <div class="form-group">
                                    <label>Modelo </label>
                                    <input type="number" v-model="vehiculo.modelo" class="form-control text-uppercase"
                                           placeholder="Modelo">
                                </div>

                                <div class="form-group">
                                    <label>Capacidad de pasajeros </label>
                                    <input type="number" max="12" v-model="vehiculo.capacidadPasajeros"
                                           class="form-control text-uppercase" placeholder="Capacidad de pasajeros">
                                </div>


                                <div class="form-group">
                                    <label>Tipo de planilla </label>

                                    <select v-model="vehiculo.tipoPlanitllaPorDefecto" class="form-control text-uppercase" required>


                                        <option value="">SELECCIONE</option>

                                        <option v-for="item in listadoTiposPlanillas" :value="item.codigo">{{item.total}}</option>


                                    </select>


                                </div>

                                <div class="form-check" v-if="operacion==='A'">

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox"  v-model="vehiculo.activo"  >
                                        <span class="form-check-sign">Activo</span>
                                    </label>
                                </div>
                                </div>

                            </div>

                        </div>


                    </div>

                </div>
                <div class="modal-footer">


                    <input type="submit" class="btn btn-primary" value="Guardar">


        </form>
    </div>
</div>
</div>




