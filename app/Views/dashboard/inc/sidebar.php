<!-- Sidebar -->
<div class="sidebar">

    <div class="sidebar-background"></div>
    <div class="sidebar-wrapper scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="<?=asset_url('img/profile_user.jpg')?>" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>



                                    <?= session('usuario') ?>

									<span class="user-level">Administrator</span>

								</span>
                    </a>
                    <div class="clearfix"></div>


                </div>
            </div>



            <ul class="nav">

<!--
                <li class="nav-item">
                    <a href="<?/*=base_url()*/?>">
                        <i class="fas fa-home"></i>
                        <p>Inicio</p>

                    </a>
                </li>-->


                <li class="nav-item">
                    <a href="<?=base_url('Administrador/vehiculos')?>">
                        <i class="fas fa-car"></i>
                        <p>Vehículos</p>

                    </a>
                </li>


                <li class="nav-item">
                    <a href="<?=base_url('Administrador/conductores')?>">
                        <i class="fas fa-user"></i>
                        <p>Conductores</p>

                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=base_url('Administrador/asociados')?>">
                        <i class="fas fa-user-tie"></i>
                        <p>Asosciados</p>

                    </a>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#base">
                        <i class="fas fa-layer-group"></i>
                        <p>Planillas</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="base">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="<?=base_url('Administrador/planillas')?>">
                                    <span class="sub-item">Planillado General</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=base_url('Administrador/planillas/especificas')?>">
                                    <span class="sub-item">Planillado Especifico</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=base_url('Administrador/planillas/descargar')?>">
                                    <span class="sub-item">Descargar</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>




                <li class="nav-item">
                    <a data-toggle="collapse" href="#forms">
                        <i class="fas fa-car-side"></i>
                        <p>Despachos</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="forms">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="<?=base_url('Administrador/despachos/registrar')?>">
                                    <span class="sub-item">Registrar</span>
                                </a>

                                <a href="<?=base_url('Administrador/despachos/consultar')?>">
                                    <span class="sub-item">Consultas</span>
                                </a>

                            </li>

                        </ul>
                    </div>
                </li>





<!--
                <li class="nav-item">
                    <a data-toggle="collapse" href="#submenu">
                        <i class="fas fa-bars"></i>
                        <p>Menu Levels</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="submenu">
                        <ul class="nav nav-collapse">
                            <li>
                                <a data-toggle="collapse" href="#subnav1">
                                    <span class="sub-item">Level 1</span>
                                    <span class="caret"></span>
                                </a>
                                <div class="collapse" id="subnav1">
                                    <ul class="nav nav-collapse subnav">
                                        <li>
                                            <a href="#">
                                                <span class="sub-item">Level 2</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="sub-item">Level 2</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a data-toggle="collapse" href="#subnav2">
                                    <span class="sub-item">Level 1</span>
                                    <span class="caret"></span>
                                </a>
                                <div class="collapse" id="subnav2">
                                    <ul class="nav nav-collapse subnav">
                                        <li>
                                            <a href="#">
                                                <span class="sub-item">Level 2</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Level 1</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>


                -->

            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->


<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">


                <h4 class="page-title"><?=$title?></h4>

                </div>
