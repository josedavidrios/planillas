<div class="row">


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-11 col-sm-11">
                <form class="navbar-left navbar-form nav-search mr-md-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-search pr-1">
                                <i class="fa fa-search search-icon"></i>
                            </button>
                        </div>
                        <input type="text" @keyup="filtar" v-model="filtro" placeholder="Buscar...."
                               class="form-control text-uppercase">

                    </div>
                </form>


            </div>


        </div>


    </div>


</div>

<br>

<div class="row">


    <div class="col-md-12">


        <div class="card">

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th width="10" scope="col">#</th>
                        <th scope="col">PLACA</th>
                        <th scope="col">CONDUCTOR</th>

                        <th scope="col">Registrar</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in listado">
                        <td>{{index+1}}</td>
                        <td>{{item.placa}}</td>
                        <td>{{item.conductor}}</td>
                        <td width="10" class="text-center">
                            <a @click="abrirModal(item.placa,item.conductor)"> <i class="fas fa-plus-circle"></i> </a>

                        </td>


                    </tr>

                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">


        <!--

        v-on:submit.prevent="registrarVehiculos"

        -->

        <form method="post" id="form" @submit.prevent="crear">

            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-md-12">


                            <div class="card-body">

                                <div class="form-group form-inline">
                                    <label class="col-md-1 col-form-label">Placa</label>


                                    <div class="col-md-2 p-0">

                                        <input type="text" readonly v-model="placa"
                                               class="form-control text-uppercase w-100"
                                               placeholder="Placa">

                                    </div>

                                    <label class="col-md-2 col-form-label">Conductor</label>


                                    <div class="col-md-7 p-0">

                                        <input type="text" v-model="conductor" class="form-control text-uppercase w-100"
                                               placeholder="Placa">

                                    </div>


                                </div>


                                <div class="form-group form-inline">
                                    <label class="col-md-1 col-form-label">Fecha</label>

                                    <div class="col-md-5 p-0">

                                        <input type="datetime" v-model="fecha"
                                               class="form-control text-uppercase w-100">

                                    </div>

                                    <label for="inlineinput" class="col-md-1 col-form-label">hora</label>

                                    <div class="col-md-5 p-0">

                                        <input type="time" v-model="hora" class="form-control text-uppercase w-100">

                                    </div>


                                </div>


                                <div class="form-group form-inline">
                                    <label class="col-md-1 col-form-label">Pasajeros </label>


                                    <div class="col-md-5 p-0">

                                        <input type="number" v-model="numeroPasajeros"
                                               class="form-control text-uppercase w-100"
                                               placeholder="Número de pasajeros">

                                    </div>

                                    <label class="col-md-1 col-form-label">Ruta </label>
                                    <div class="col-md-5 p-0">


                                        <select class="form-control text-uppercase w-100" required name=""
                                                v-model="ruta" id="">

                                            <option value="">SELECCIONE</option>
                                            <option value="TS">TOLUVIEJO - SINCELEJO</option>
                                            <option value="ST">SINCELEJO - TOLUVIEJO</option>

                                        </select>

                                    </div>


                                </div>

                            </div>


                        </div>

                    </div>
                    <div class="modal-footer">


                        <input type="submit" class="btn btn-primary" value="Registrar">


        </form>
    </div>
</div>
</div>


<script>


    const controlador = "Vehiculo/";

    new Vue({
        el: '#app',


        created: function () {

            this.listar();

        },

        data: {

            placa: '',
            listado: [],
            filtro: '',
            ruta: '',
            numeroPasajeros: '',
            fecha: new Date().toISOString().slice(0, 10),
            hora: '',
            conductor: ''


        },
        methods: {

            listar: function () {


                axios.get(BASE_URL + controlador + 'consultarInformacionConductor').then(response => {
                    this.listado = response.data;
                });


            },

            filtar: function () {


                const params = new FormData();
                params.append('placa', this.filtro);

                axios.post(BASE_URL + controlador + 'filtrar', params).then(response => {

                    this.listado = response.data;
                });

            },
            crear: function (event) {


                const params = new FormData();

                params.append('placaVehiculo', this.placa);
                params.append('numeroPasajeros', this.numeroPasajeros);
                params.append('fecha', this.fecha);
                params.append('numeroPasajeros', this.numeroPasajeros);
                params.append('ruta', this.ruta);
                params.append('hora', this.hora);


                axios.post(BASE_URL + 'Despacho/crear', params
                ).then(response => {


                    //   if (response.data == 1) {

                    $('#exampleModal').modal('toggle');

                    swal({
                        title: "Mensaje",
                        text: "El usuarios se ha creadp correctamente",
                        button: "Aceptar",
                    });

                    event.target.reset();


                    this.filtar();

                    //   }


                    this.listar();


                }).catch(error => {


                    swal({
                        title: "Mensaje",
                        text: "Error",
                        button: "Aceptar",
                        icon: 'error',
                    });

                });


            }, abrirModal: function (placa, conductor) {

                if (!placa == 0) {

                    this.placa = placa;
                    this.conductor = conductor;


                } else {

                    $('#form')[0].reset();

                }

                $('#exampleModal').modal('toggle');

            },

        }
    });


</script>



