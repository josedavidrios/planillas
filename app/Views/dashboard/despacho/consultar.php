<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Base Form Control</div>
            </div>
            <div class="card-body">


                <form @submit.prevent="consultar" target="_blank" method="post">

                <div class="form-group form-inline">
                    <label for="inlineinput" class="col-md-1 col-form-label">Vehúculo</label>

                    <div class="col-md-7 p-0">

                        <v-select   :required="!placa"    :required="required" v-model="placa"  :options="listado"></v-select>


                        <input type="hidden"  required v-model="placa" />




                    </div>

                    <label class="col-md-1 col-form-label">Fecha</label>


                    <div class="col-md-2 p-0">

                        <input type="date" v-model="fecha" required class="form-control">

                    </div>








                    <div class="col-md-1">
                        <input type="submit" class="btn btn-success" value="Descargar">
                    </div>

                </div>


               </form>

            </div>

        </div>

    </div>

</div>

<div class="row">


    <div class="col-md-12">


        <div class="card">


            <!--
            <div class="card-header">
                <div class="card-title">Hoverable Table</div>
            </div>

            -->

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th width="10" scope="col">#</th>
						<th  scope="col">RUTA</th>
                        <th class="text-center" width="20" scope="col">PASAJEROS</th>



                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in despachos">

                        <td>{{index+1}}</td>
                        <td>{{item.nombreRuta}}</td>
                        <td class="text-center">{{item.numeroPasajeros}}</td>

                   </tr>


					<tr v-if="total!=0">
						<td></td>
						<td> <strong>TOTAL</strong></td>
						<td class="text-center"><strong>{{total}}</strong></td>


					</tr>

                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>
