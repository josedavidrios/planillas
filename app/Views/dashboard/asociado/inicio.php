<div class="row">


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-11 col-sm-11">
                <form class="navbar-left navbar-form nav-search mr-md-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-search pr-1">
                                <i class="fa fa-search search-icon"></i>
                            </button>
                        </div>
                        <input type="text" @keyup="filtrar" v-model="filtro" placeholder="Buscar...."
                               class="form-control text-uppercase">

                    </div>
                </form>


            </div>

            <div class="col-md-1 col-sm-1">


                <a href="#" class="pull-right btn btn-info btn-border btn-round btn-sm" data-toggle="modal" @click="abrirModal(0)">
                    <span class="btn-label"><i class="fa fa-user-plus"></i></span>Nuevo
                </a>


            </div>

        </div>


    </div>


</div>

<br>

<div class="row">


    <div class="col-md-12">


        <div class="card">


            <!--
            <div class="card-header">
                <div class="card-title">Hoverable Table</div>
            </div>

            -->

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th width="10" scope="col">#</th>
                        <th scope="col">IDENTIFICACIÓN</th>
                        <th scope="col">NOMBRES</th>
            <!--            <th class="text-center"  scope="col">ACTIVO</th>-->
                        <th scope="col">EDITAR</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in listado">
                        <td>{{index+1}}</td>
                        <td>{{item.identificacion}}</td>
                        <td>{{item.nombres}}</td>
<!--                        <td class="text-center" v-html="item.activo"></td>-->

                        <td width="10" class="text-center">

                            <a @click="abrirModal(item.identificacion)"> <i class="fas fa-edit"></i> </a>

                        </td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">


        <!--

        v-on:submit.prevent="registrarVehiculos"

        -->

        <form method="post" id="form" @submit.prevent="crear">

            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-md-12">


                            <div class="card-body">


                                <div class="form-group">
                                    <label>Identificación</label>

                                    <input type="hidden" v-model="asociado.codigo" required class="form-control text-uppercase">


                                    <input type="text" v-model="asociado.identificacion" class="form-control text-uppercase"
                                           placeholder="Identificación">



                                </div>

                                <div class="form-group">
                                    <label>Nombres</label>
                                    <input type="text" v-model="asociado.nombres" required class="form-control text-uppercase"
                                           placeholder="Nombres completos">

                                </div>

								<div class="form-check" v-if="operacion==='A'">
                                    <label class="form-check-label">
                                        <input class="form-check-input" v-model="asociado.activo" type="checkbox"  :checked="isChecked"  >
                                        <span class="form-check-sign">Activo</span>
                                    </label>
                                </div>


                            </div>

                        </div>


                    </div>

                </div>
                <div class="modal-footer">


                    <input type="submit" class="btn btn-primary" value="Guardar">


        </form>
    </div>
</div>
</div>


<script>



    const controlador = "Asociado/";

    new Vue({
        el: '#app',


        created: function () {

            this.listar();

        },

        data: {

            listado: [],
            filtro: '',
            isChecked:true,
			operacion:'C',


            asociado:{

                codigo:'',
                identificacion: '',
                nombres: '',
                activo:1

            }



        },
        methods: {

            listar: function () {


                axios.get(BASE_URL +controlador+'mostrar').then(response => {
                    this.listado = response.data;
                });

            },

            filtrar: function (event) {


              const params = new FormData();
              params.append('nombres', event.target.value);


                axios.post(BASE_URL + controlador+'filtrar', params).then(response => {

                    this.listado = response.data;
                });

            },


            consultar: function (identificacion) {

                const params = new FormData();
                params.append('identificacion', identificacion);

                axios.post(BASE_URL + controlador+'mostrar', params).then(response => {

                    //this.vehiculo=response.data;

                	this.asociado.codigo = response.data[0].codigo;
                    this.asociado.identificacion = response.data[0].identificacion;
                    this.asociado.nombres = response.data[0].nombres;
                    this.asociado.activo = response.data[0].activo;




                    console.log("x "+response.data[0].activo);


                    console.log("Estado "+this.asociado.activo);




                    if (this.asociado.activo==0){

                        this.asociado.activo =0;
                       // this.isChecked=false;



                    }



                });

            },
            crear: function (event) {


                const params = new FormData();


                var mjs="El asociado se ha registrado exitosamente";

                if (this.operacion=="A"){

                    mjs = "El asociado se ha actualizado exitosamente";

                    params.append('codigo', this.asociado.codigo);


                }

                params.append('identificacion', this.asociado.identificacion);
                params.append('nombres', this.asociado.nombres);
                params.append('activo', this.asociado.activo);


                axios.post(BASE_URL + controlador+'crear', params).then(response => {


                    if (response.data != "0") {

                        $('#exampleModal').modal('toggle');

                        swal({
                            title: "Mensaje",
                            text: mjs,
                            button: "Aceptar",
                        });

                        event.target.reset();
                    }


                    this.listar();


                    console.log(response.data);


                }).catch(error => {


                    swal({
                        title: "Mensaje",
                        text: "Error",
                        button: "Aceptar",
                        icon: 'error',
                    });

                });


            }, abrirModal: function (identificacion) {


                if (!identificacion==0){

                    this.consultar(identificacion);
                    this.operacion='A';

                } else{


                    $('#form')[0].reset();

                    this.isChecked=true;

                }




                $('#exampleModal').modal('toggle');

            },

        }
    });


</script>



