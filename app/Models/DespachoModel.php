<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class DespachoModel extends Model
{


    protected $table      = 'despachos';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['ruta','placaVehiculo','hora', 'fecha','numeroPasajeros','fechaRegistro'];




    function consultar($placa=null, $fecha=null){


        $this->select("despachos.numeroPasajeros, rutas.nombre AS nombreRuta")
            ->join("rutas","rutas.codigo=despachos.ruta","INNER");




        if (!is_null($placa)){


            $this->where("placaVehiculo",$placa);


        }

        if (!is_null($fecha)){

            $this->where("fecha",$fecha);
        }


        return $this->findAll();



    }



}
