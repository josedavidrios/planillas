<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class SesionModel extends Model
{


    protected $table      = 'admistradores';



    function iniciarSesion($usuario,$clave){


        $this->where("identificacion",$usuario)
             ->where("clave",$clave);


        return $this->asObject()->findAll();


    }


}