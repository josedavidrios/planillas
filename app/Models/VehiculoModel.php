<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class VehiculoModel extends Model
{


    protected $table = 'vehiculos';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['placa', 'tipo', 'marca', 'identificacionConductorPorDefecto', 'tipoPlanitllaPorDefecto', 'nombres', 'modelo', 'capacidadPasajeros', 'activo', 'fechaRegistro', 'ultimaFechaActualizacion', 'tarjetaPropiedad'];


    function consultar($placa = null)
    {


        if (!is_null($placa)) {

            $this->where("placa", $placa);


        }


        return $this->where("activo", 1)->orderBy('placa', 'asc')->findAll();


    }


    function filtrar($placa = null)
    {


        $this->select("vehiculos.placa, c.nombres as conductor, vehiculos.marca, vehiculos.tipo")
            ->join("conductores c", "c.identificacion = vehiculos.identificacionConductorPorDefecto", "INNER")
            ->where("vehiculos.activo", 1);


        strcmp($placa, "all") != 0 ?   $this->like('placa', $placa, 'both'): "";




        return $this->findAll();


    }


}
