<?php

namespace App\Models;


use CodeIgniter\Model;

class PlanillaModel extends Model
{


    protected $table      = 'planillas';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['fecha','hora','identificacionConductor', 'placaVehiculo','tipo','pagada','fechaRegistro'];


    function consultar($numero=null){


        if (!is_null($numero)){


            $this->where("numero",$numero);


        }

        return $this->findAll();


    }


    function crear($dat){



        return $this->insert($dat);



    }


    function filtrar($placa){


      //  return $this->where("pagada",0)->like('placaVehiculo',$nombre,'both')->findAll();

        return $this->like('placaVehiculo',$placa,'both')->findAll();


	}


	function consultarPor($campo="placa",$valor){

        $this->select("planillas.numero, planillas.fecha, r.nombre As ruta, c.nombres AS conductor")
			 ->join("detalleplanillas dp","dp.numeroPlanilla=planillas.numero","INNER")
			 ->join("rutas r","dp.codigoRuta=r.codigo","INNER")
        	 ->join("conductores c","c.identificacion=planillas.identificacionConductor","INNER")
             ->where($campo,$valor);


        return $this->findAll();


    }


    function crearDetallePlanilla($dat){


        $db      = \Config\Database::connect();
        $builder = $db->table('detalleplanillas');
        $builder->insert($dat);



    }


    function consultarPlanillado($fecha){


        $this->select(" tiposplanillas.total AS valor, COUNT(tiposplanillas.codigo) AS cant,  tiposplanillas.total* COUNT(tiposplanillas.codigo) AS total")
             ->join("tiposplanillas","tiposplanillas.codigo = planillas.tipo","INNER")
            ->groupBy("planillas.tipo")
            ->where("planillas.fecha",$fecha);


        return $this->findAll();
    }



    function consultarTiposPlanillas(){


        $db      = \Config\Database::connect();
        $builder = $db->table('tiposplanillas');
        return  $builder->select('codigo, FORMAT(total,0) as total')->where("anio",date('Y'))->get()->getResultObject();

    }


    function consultarUsuariosCausarsion(){


        //u.codigo, u.nombres, u.direccion,e.estrato, e.codigo AS codigo_estrato, u.codigo_tarifa

        $this->select("u.codigo,
                             u.nombres,
                             u.direccion,
                             u.codigoEstrato, 
                             u.codigoTarifa,
                             t.codigo AS codigoTarifa,
                             u.codigoEstrato,
                             
                             
                             
                             tac.total  AS valorTarifaAcueducto, tal.total AS valorTarifaAlcantarillado,tas.total AS valorTarifaAseo",true)



            ->join("barrios b","b.codigo = u.codigoBarrio")
            ->join("estratos e","e.codigo = u.codigoEstrato")
            ->join("tarifas t","t.codigo = u.codigoTarifa")

            ->join("tarifasAcueducto tac","tac.codigo = t.tarifaAcueducto")
            ->join("tarifasAlcantarillado tal","tal.codigo = t.tarifaAlcantarillado")
            ->join("tarifasAseo tas","tas.codigo = t.tarifaAseo")


            ->where("u.causar",1);




        return $this->findAll();

    }



    function causar(){



    }








}
