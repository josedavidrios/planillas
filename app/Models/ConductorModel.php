<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class ConductorModel extends Model
{


    protected $table      = 'conductores';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['identificacion','nombres', 'fechaNacimiento','activo','licencia','categoriaLicencia','vigenciaLicencia','fechaRegistro','ultimaFechaActualizacion'];


    function consultar($identificacion=null, $activo=null){


        if (!is_null($identificacion)){


            $this->where("identificacion",$identificacion);


        }

        !is_null($activo)? $this->where("activo",1) :'';


        return $this->orderBy('activo','DESC')->orderBy("nombres")->findAll();


    }


    function filtrar($nombre){


    	return $this->where("activo",1)->like('nombres',$nombre,'both')->findAll();


	}


    function consultarUsuariosCausarsion(){


        //u.codigo, u.nombres, u.direccion,e.estrato, e.codigo AS codigo_estrato, u.codigo_tarifa

        $this->select("u.codigo,
                             u.nombres,
                             u.direccion,
                             u.codigoEstrato, 
                             u.codigoTarifa,
                             t.codigo AS codigoTarifa,
                             u.codigoEstrato,
                             
                             
                             
                             tac.total  AS valorTarifaAcueducto, tal.total AS valorTarifaAlcantarillado,tas.total AS valorTarifaAseo",true)



            ->join("barrios b","b.codigo = u.codigoBarrio")
            ->join("estratos e","e.codigo = u.codigoEstrato")
            ->join("tarifas t","t.codigo = u.codigoTarifa")

            ->join("tarifasAcueducto tac","tac.codigo = t.tarifaAcueducto")
            ->join("tarifasAlcantarillado tal","tal.codigo = t.tarifaAlcantarillado")
            ->join("tarifasAseo tas","tas.codigo = t.tarifaAseo")


            ->where("u.causar",1);




        return $this->findAll();

    }



    function causar(){



    }








}
