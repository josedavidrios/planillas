const controlador = "Vehiculo/";

new Vue({
    el: '#app',


    created: function () {

        this.listar();


    },

    data: {


        marcas: ['NISSAN', 'JEEP', 'TOYOTA'],
        tipos: ['CAMPERO', 'CAMIONETA'],

        operacion: 'R',

        listado: [],
        listadoConductores: [],
        filtro: '',
        listadoTiposPlanillas: [],


        vehiculo: {

            codigo: '',
            placa: '',
            marca: '',
            tipo: '',
            modelo: '',
            capacidadPasajeros: '',
            tarjetaPropiedad: '',
            tipoPlanitllaPorDefecto: '',
            conductorPorDefecto: '',
            activo: ''

        }


    },
    methods: {




        listar: function () {


            axios.get(BASE_URL + controlador + 'listar').then(response => {

                this.listado = response.data;

                console.log(this.listado);

            });


        },

        filtar: function () {


            const params = new FormData();


            if (this.filtro.length > 2) {

                params.append('placa', this.filtro);

                axios.post(BASE_URL + controlador + 'filtrar', params).then(response => {

                    this.listado = response.data;
                });

            }else{

                this.listado=[];
            }

        },


        consultar: function (placa) {

            const params = new FormData();
            params.append('placa', placa);


            axios.post(BASE_URL + controlador + 'mostrar', params).then(response => {


                this.vehiculo.codigo = response.data[0].codigo;
                this.vehiculo.placa = response.data[0].placa;
                this.vehiculo.tarjetaPropiedad = response.data[0].tarjetaPropiedad;
                this.vehiculo.marca = response.data[0].marca;
                this.vehiculo.tipo = response.data[0].tipo;
                this.vehiculo.modelo = response.data[0].modelo;
                this.vehiculo.capacidadPasajeros = response.data[0].capacidadPasajeros;
                this.vehiculo.tipoPlanitllaPorDefecto = response.data[0].tipoPlanitllaPorDefecto;
                this.vehiculo.conductorPorDefecto = response.data[0].identificacionConductorPorDefecto;
                this.vehiculo.activo = response.data[0].activo;

            });

        },
        crear: function (event) {


            const params = new FormData();

            var mjs = "El vehículo se ha registrado exitosamente";

            if (this.operacion === "A") {

                mjs = "El vehículo se ha actualizado exitosamente";

                params.append('codigo', this.vehiculo.codigo);


            }


            // Validación del estado de vehículo
            !this.vehiculo.activo ?  this.vehiculo.activo = 0 :  this.vehiculo.activo=1;


            params.append('placa', this.vehiculo.placa);
            params.append('marca', this.vehiculo.marca);
            params.append('tipo', this.vehiculo.tipo);
            params.append('modelo', this.vehiculo.modelo);
            params.append('capacidadPasajeros', this.vehiculo.capacidadPasajeros);
            params.append('tarjetaPropiedad', this.vehiculo.tarjetaPropiedad);
            params.append('tipoPlanitllaPorDefecto', this.vehiculo.tipoPlanitllaPorDefecto);
            params.append('identificacionConductorPorDefecto', this.vehiculo.conductorPorDefecto);
            params.append('activo', this.vehiculo.activo);

            axios.post(BASE_URL + controlador + 'crear', params).then(response => {



                if (response.data !== "0") {


                    $('#exampleModal').modal('toggle');

                    swal({
                        title: "Mensaje",
                        text: mjs,
                        button: "Aceptar",
                    });

                    event.target.reset();
                }

                this.operacion = "C";
                this.listar();


            }).catch(error => {


                swal({
                    title: "Mensaje",
                    text: "Error",
                    button: "Aceptar",
                    icon: 'error',
                });

            });


        }, abrirModal: function (placa) {


            this.listarConductores();
            this.listarTiposPlanillas();

            if (placa !== 0) {

                this.consultar(placa);
                this.operacion = "A";

            } else {


                $('#form')[0].reset();
                this.vehiculo=[];
                this.vehiculo.activo =1;


            }



            $('#exampleModal').modal('toggle');


        },

        listarConductores: function () {

            const params = new FormData();


            params.append('activo', 1);

            axios.get(BASE_URL + 'Conductor/mostrar').then(response => {
                this.listadoConductores = response.data;
            });

        },

        listarTiposPlanillas: function () {


            axios.get(BASE_URL + 'Planilla/mostrarTiposPlanillas').then(response => {

                this.listadoTiposPlanillas = response.data;


            });

        },

    }
});
