Vue.component('v-select', VueSelect.VueSelect);

const controlador = "Vehiculo/";

new Vue({
    el: '#app',

    created: function () {

        this.listarPlacas();

    },

    data: {

        selected: '',
        listado: []
    },


    methods: {

        listarPlacas: function () {

            axios.get(BASE_URL + controlador + 'mostrarPlacas').then(response => {
                this.listado = response.data;
            });

        }


    }
});