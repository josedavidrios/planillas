


const controlador = "Conductor/";

new Vue({
    el: '#app',


    created: function () {

        this.listar();

    },

    data: {

        listado: [],
        filtro: '',
        operacion: 'R',


        conductor: {

            codigo: '',
            identificacion: '',
            nombres: '',
            fechaNacimiento: '',
            licencia: '',
            vigenciaLicencia: '',
            categoriaLicencia: '',
            activo: ''

        }


    },
    methods: {

        listar: function () {


            axios.get(BASE_URL + controlador + 'mostrarTodo').then(response => {
                this.listado = response.data;

             //   console.log( this.listado);

            });

        },

        filtrar: function () {


            const params = new FormData();
            params.append('nombres', this.filtro);


            axios.post(BASE_URL + controlador + 'filtrar', params).then(response => {

                this.listado = response.data;
            });

        },


        consultar: function (identificacion) {

            const params = new FormData();
            params.append('identificacion', identificacion);

            axios.post(BASE_URL + controlador + 'mostrar', params).then(response => {

                //this.vehiculo=response.data;




                this.conductor.codigo = response.data[0].codigo;
                this.conductor.identificacion = response.data[0].identificacion;
                this.conductor.nombres = response.data[0].nombres;
                this.conductor.fechaNacimiento = response.data[0].fechaNacimiento;
                this.conductor.activo = response.data[0].activo;

                this.conductor.licencia = response.data[0].licencia;
                this.conductor.vigenciaLicencia = response.data[0].vigenciaLicencia;
                this.conductor.categoriaLicencia = response.data[0].categoriaLicencia;

                this.conductor.activo =response.data[0].activo;

               this.conductor.activo ==0? this.conductor.activo="" : '';




            });

        },
        crear: function (event) {

            const params = new FormData();


            var mjs = "El conductor se ha registrado exitosamente";

            if (this.operacion === "A") {

                mjs = "El conductor se ha actualizado exitosamente";

                params.append('codigo', this.conductor.codigo);


            }


            params.append('identificacion', this.conductor.identificacion);
            params.append('nombres', this.conductor.nombres);
            params.append('fechaNacimiento', this.conductor.fechaNacimiento);
            params.append('licencia', this.conductor.licencia);
            params.append('vigenciaLicencia', this.conductor.vigenciaLicencia);
            params.append('categoriaLicencia', this.conductor.categoriaLicencia);



            // Validación del estado de conductor
            !this.conductor.activo ?  this.conductor.activo = 0 :  this.conductor.activo=1;

            params.append('activo', this.conductor.activo);


            axios.post(BASE_URL + controlador + 'crear', params).then(response => {


                if (response.data != "0") {

                    $('#exampleModal').modal('toggle');

                    swal({
                        title: "Mensaje",
                        text: mjs,
                        button: "Aceptar",
                    });


                }


                this.listar();

                event.target.reset();
                this.operacion = "C";


            }).catch(error => {


                swal({
                    title: "Mensaje",
                    text: "Error",
                    button: "Aceptar",
                    icon: 'error',
                });

            });


        }, abrirModal: function (identificacion) {


            if (!identificacion == 0) {

                this.operacion = 'A';
                this.consultar(identificacion);


            } else {


                $('#form')[0].reset();



            }


            console.log(identificacion);


            $('#exampleModal').modal('toggle');

        },

    }
});


